from pydub import AudioSegment
from PIL import Image
import scipy.io.wavfile
from scipy.misc import imsave
import numpy as np

im = Image.open("bach.jpg")
im2 = im.convert("L") #Переводим в серое изображение
#im2.show()
jpgdata = np.asarray(im2.getdata(), dtype=np.int16)
#print(jpgdata)
scipy.io.wavfile.write('bach.wav', 16000, jpgdata) #Не работает битрейт =/

song = AudioSegment.from_wav("bach.wav")
song.export("bach.mp3", format="mp3", bitrate="320k") #Крутить тут

song2 = AudioSegment.from_mp3("bach.mp3")
song2.export("bach2.wav", format="wav")
jpg2data = np.asarray(scipy.io.wavfile.read("bach2.wav")[1], dtype=np.uint8)
jpg2reshaped = np.reshape(jpg2data, (565, 439))
im3 = Image.fromarray(jpg2reshaped)
im3.save("Bach2.jpg")