Image compression with MP3 algorythm

Image turn to grayscale mode, then convert to WAV format. After that WAV file compress with MP3 algorythm. Then turn back to WAV and back to JPG.

Used libraries: 

<B>Pillow</B> (for working with images)

<B>PyDub</B> (for working with audio formats)

<B>Numpy</B> & <B>SciPy</B> for transfering data between libraries

Original image:

![Original image](bach.jpg)

Modified (48kHz, 4kb/s)

![Original image](48_4.jpg)

Modified (24kHz, 2kb/s)

![Original image](24_2.jpg)

Modified (16kHz, 1kb/s)

![Original image](16_1.jpg)